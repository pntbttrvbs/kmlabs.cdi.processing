#make sure to use the ptycho Py 2.7 env!

import ptypy
from ptypy import utils as u
from ptypy.core import View, Ptycho, POD, geometry, xy, Container
from ptypy.experiment import SimScan
from matplotlib import pyplot as plt
import numpy as np
import KMutils
import os

import sys
scriptname = sys.argv[0].split('//')[-1].split('.')[-2]
print('/n')
print(scriptname)
print('/n')

if not os.path.exists('images/'):
    os.mkdir('images/')

p = u.Param(verbose_level = 5,
            data_type = 'single')
"""
p.verbose_level = 5
#p.ipython_kernel = 'True' #not working - WHY.
p.data_type = "single"
"""
"""
p.engines = u.Param()
p.engines.engine00 = u.Param()
p.engines.engine00.name = 'DM'
p.engines.engine00.numiter = 40
p.engines.engine00.numiter_contiguous = 5
"""

P = Ptycho(p)

g = u.Param()
g.energy = None #needs to be explicitly declared b/c of default value (energy takes prescidence over wavelength)
g.lam = 3e-8 #30nm to 13nm;
g.distance = 50e-3 #50 for shoebox, 40 for mask.
g.psize = 13.5e-6
g.shape = 2048
g.propagation = 'farfield' #but... it's pretty close, isn't it? maybe run simulations under both condition
G = geometry.Geo(pars = g)

#this is just some leftover checks from the tutorial...
#print(G.resolution)

fsize = G.shape*G.resolution
print(G.shape)
print(G.resolution)
print("%.2fx%.2fmm"% tuple(fsize*1e3))
#
#why this shape?
pr_shape = (1,)+ tuple(G.shape)

#try fucking around with the probe?? idk.
#pr_shape = (1,)+ tuple(256,256)
pr = P.probe.new_storage(shape=pr_shape, psize=G.resolution)

y, x = G.propagator.grids_sam # what is this doing
#apert = u.smooth_step(fsize[0]/5-np.sqrt(x**2+y**2), 1e-6) #this is a dot, I think 20% of the frame size?

probeWidth = 5e-6
probeHeight = 10e-6
apert = u.smooth_step(1 - np.sqrt(((x**2) / ((probeWidth / 2) ** 2)) + ((y ** 2) / ((probeHeight / 2) ** 2))), 1e-6)

print(apert.shape)

probeOverlap = 0.8

pr.fill(apert)
photons = 1e9 #1e9 photons @ sample for shoebox, 1e8 for mask
pr.data *= np.sqrt(photons/np.sum(pr.data*pr.data.conj()))

fig = u.plot_storage(pr,1,channel='c')
fig.savefig('images/probe.png', dpi=300)

#ill = pr.data[0]
#propagated_ill = G.propagator.fw(ill)
#fig = plt.figure(3)
#ax = fig.add_subplot(111)
#im = ax.imshow(np.log10(np.abs(propagated_ill)+1))
#plt.colorbar(im)
#fig.savefig('propogated_ill.png', dpi=300)

#check these scan pattern parameters
pos = u.Param()
pos.model = 'round'
#pos.spacing = fsize[0]/8
#pos.spacing = KMutils.calcStepSize(min(probeWidth, probeHeight)/2,probeOverlap)
pos.spacing = KMutils.calcStepSize(probeHeight/2,probeOverlap)
#pos.steps = 50
pos.steps = None
#find a better way to figure out extent - may just number of steps (above)?
#pos.extent = fsize*0.5
positions = xy.from_pars(pos)
fig = plt.figure(4)
ax = fig.add_subplot(111)
ax.plot(positions[:,1], positions[:, 0], 'o-')
fig.savefig('images/scanpattern-{}.png'.format(g.shape), dpi=300)

oar = View.DEFAULT_ACCESSRULE.copy()
oar.storageID = 'S00'
oar.psize = G.resolution
oar.layer = 0
oar.shape = G.shape
oar.active = True

for pos in positions:
    r = oar.copy()
    r.coord = pos
    V = View(P.obj, None, r)

#print(P.obj.formatted_report())
P.obj.reformat()
#print(P.obj.formatted_report())

#need to figure out how to import regular images...
from ptypy.resources import flower_obj
storage = P.obj.storages['S00']
#print(flower_obj(storage.shape[-2:]).shape)
storage.fill(flower_obj(storage.shape[-2:]))
#storage.fill('cameraman.png') # 'a float is required...
fig = u.plot_storage(storage, 5)
fig.savefig('images/object-{}.png'.format(g.shape) , dpi=300)

#todo - I'd love to subclass get_view_coverage for probes
#Test_C = Container(data_type='int')
#View_Coverage = Test_C.new_storage(shape=storage.shape)
#View_Coverage.data[:] = storage.get_view_coverage()
#fig = u.plot_storage(View_Coverage,1000)
#fig.savefig('images/coverage.png', dpi=300)

objviews = P.obj.views.values()

probe_ar = View.DEFAULT_ACCESSRULE.copy()
probe_ar.psize = G.resolution
probe_ar.shape = G.shape
probe_ar.active = True
probe_ar.storageID = pr.ID
prview = View(P.probe, None, probe_ar)

exit_ar = probe_ar.copy()
exit_ar.layer = -1

diff_ar = probe_ar.copy()
diff_ar.layer = -1
diff_ar.psize = G.psize
mask_ar = diff_ar.copy()

pods = []


for obview in objviews:
    #prview = View(P.probe, None, probe_ar) #duplicate line?
    exit_ar.layer += 1
    diff_ar.layer += 1
    exview = View(P.exit, None, exit_ar)
    maview = View(P.mask, None, mask_ar)
    diview = View(P.diff, None, diff_ar)
    views = {'probe': prview,
             'obj': obview,
             'exit': exview,
             'diff': diview,
             'mask': maview}
    pod = POD(P, model=None, ID=None, views=views, geometry=G)
    pods.append(pod)

#throwing memory error on laptop... definitely using 64 bit python, may be a good excuse to try py3.
for C in [P.mask, P.exit, P.diff, P.probe]:
    #print(C.formatted_report())
    C.reformat()
    #print(C.formatted_report())

for pod in pods:
    pod.exit = pod.probe * pod.object
    pod.diff = np.random.poisson(np.abs(pod.fw(pod.exit))**2)
    pod.mask = np.ones_like(pod.diff)


#fig = plt.figure()
#fig = u.plot_storage(P.exit.storages.values()[0])
#fig.savefig('images/exit_%d' % (fig.number))

diff_storage = P.diff.storages.values()[0]
fig = u.plot_storage(diff_storage, 8, slices=':2,:,:', modulus='log')
fig.savefig('diffraction_patterns.png', dpi=300)

#storing
save_path = '/tmp/ptypy/sim/'
if not os.path.exists(save_path):
    os.makedirs(save_path)

# basically, don't generate sim data if it doesn't already exist.
if 'geometry.txt' not in os.listdir(save_path):

    # First, we save the geometric info in a text file.
    with open(save_path+'geometry.txt', 'w') as f:
        f.write('distance %.4e\n' % G.p.distance)
        f.write('energy %.4e\n' % G.energy)
        f.write('psize %.4e\n' % G.psize[0])
        f.write('shape %d\n' % G.shape[0])
        f.close()

    #this should guide what the software should create...
    with open(save_path+'positions.txt', 'w') as f:
        if not os.path.exists(save_path+'ccd/'):
            os.mkdir(save_path+'ccd/')

        #add our unique probe:

        probe_frame = 'ccd/probe.npy'
        f.write(probe_frame)
        frame = pods[0].probe.astype(np.complex64)
        np.save(save_path+probe_frame, frame)

        for pod in pods:
            diff_frame = 'ccd/diffraction_%04d.npy' % pod.di_view.layer
            f.write(diff_frame+' %.4e %.4e\n' % tuple(pod.ob_view.coord))
            frame = pod.diff.astype(np.int32)
            np.save(save_path+diff_frame, frame)




"""
P.p.scans = u.Param()
P.p.scans.name = 'KM'
P.p.scans.KM = u.Param()
P.p.scans.KM.name = 'Full'
#P.p.scans.KM.data = u.Param()
#P.p.scans.KM.data.name = 'SimScan'



P.init_engine()
P.run()
fig = u.plot_storage(P.obj.S.values()[0], 20, slices='0,120:-120,120:-120')
fig.savefig('reconstruction_orig.png', dpi=300)
"""
