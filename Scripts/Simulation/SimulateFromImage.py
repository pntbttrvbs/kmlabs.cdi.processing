import os
from ptypy import utils as u
from ptypy.core import geometry

#inputs
sourceImage = 'place image here'
diffractionDirectory = '/path/to/data'
title = 'how am I naming these'
diffractionDirectory = os.getcwd + title

#ptycho instance parameters
p = u.Param()
p.verbose_level = 1 # for batch, I can put this pretty low to save on processing power.
p.data_type = 'single'

g = u.Param()
g.energy = None
g.lam = 30e-9
g.distance = 50e-3
g.psize = 13.5e-6
g.shape = 2048
g. propagation = 'farfield'
G = geometry.Geo(pars = g)

