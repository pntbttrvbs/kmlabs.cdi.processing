import ptypy
from ptypy import utils as u
import numpy as np

p = u.Param()
p.verbose_level = 5
p.data_type = "single"

p.scans = u.Param()
p.scans.KM = u.Param()
p.scans.KM.name = 'Full'
p.scans.KM.data = u.Param()
p.scans.KM.data.name = 'NumpyScan'
p.scans.KM.data.shape = 128
p.scans.KM.data.energy = 4.1328e-2
#p.scans.KM.data.wavelength = 30e-9
p.scans.KM.data.distance = 5e-2
p.scans.KM.data.psize = 1.35e-05
#p.scans.MF.data.num_frames = 400

P = ptypy.core.Ptycho(p, level=2)

diff_storage = P.diff.storages.values()[0]
fig = u.plot_storage(diff_storage, 0, slices=(slice(2), slice(None), slice(None)), modulus='log')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)

def fourier_update(pods):
    import numpy as np
    pod = pods.values()[0]
    # Get Magnitude and Mask
    mask = pod.mask
    modulus = np.sqrt(np.abs(pod.diff))
    # Create temporary buffers
    Imodel = np.zeros_like(pod.diff)
    err = 0.
    Dphi = {}
    # Propagate the exit waves
    for gamma, pod in pods.iteritems():
        Dphi[gamma] = pod.fw(2*pod.probe*pod.object - pod.exit)
        Imodel += np.abs(Dphi[gamma] * Dphi[gamma].conj())
    # Calculate common correction factor
    factor = (1-mask) + mask * modulus / (np.sqrt(Imodel) + 1e-10)
    # Apply correction and propagate back
    for gamma, pod in pods.iteritems():
        df = pod.bw(factor*Dphi[gamma]) - pod.probe*pod.object
        pod.exit += df
        err += np.mean(np.abs(df*df.conj()))
    # Return difference map error on exit waves
    return err

def probe_update(probe, norm, pods, fill=0.):
    """
    Updates `probe`. A portion `fill` of the probe is kept from
    iteration to iteration. Requires `norm` buffer and pod dictionary
    """
    probe *= fill
    norm << fill + 1e-10
    for name, pod in pods.iteritems():
        if not pod.active: continue
        probe[pod.pr_view] += pod.object.conj() * pod.exit
        norm[pod.pr_view] += pod.object * pod.object.conj()
    # For parallel usage (MPI) we have to communicate the buffer arrays
    probe.allreduce()
    norm.allreduce()
    probe /= norm

def object_update(obj, norm, pods, fill=0.):
    """
    Updates `object`. A portion `fill` of the object is kept from
    iteration to iteration. Requires `norm` buffer and pod dictionary
    """
    obj *= fill
    norm << fill + 1e-10
    for pod in pods.itervalues():
        if not pod.active: continue
        pod.object += pod.probe.conj() * pod.exit
        norm[pod.ob_view] += pod.probe * pod.probe.conj()
    obj.allreduce()
    norm.allreduce()
    obj /= norm

def iterate(Ptycho, num):
    # generate container copies
    obj_norm = P.obj.copy(fill=0.)
    probe_norm = P.probe.copy(fill=0.)
    errors = []
    for i in range(num):
        err = 0
        # fourier update
        for di_view in Ptycho.diff.V.itervalues():
            if not di_view.active: continue
            err += fourier_update(di_view.pods)
        # probe update
        probe_update(Ptycho.probe, probe_norm, Ptycho.pods)
        # object update
        object_update(Ptycho.obj, obj_norm, Ptycho.pods)
        # print error
        errors.append(err)
        if i % 3==0: print err
    # cleanup
    P.obj.delete_copy()
    P.probe.delete_copy()
    #return error
    return errors


probe_storage = P.probe.storages.values()[0]
#fig = u.plot_storage(P.probe.S.values()[0], 1)
#fig.savefig('std_probe.png', dpi=300)
# Plot of the starting guess for the probe.

#This changes the probe to the probe that's loaded in the ptyscan (numpyscan)
probe_storage.fill(P.model.scans.values()[0].ptyscan.pr)

fig = u.plot_storage(P.probe.S.values()[0], 200)
fig.savefig('init_probe.png', dpi=300)

fig = u.plot_storage(P.obj.S.values()[0], 201, slices='0,120:-120,120:-120')
fig.savefig('init_obj.png', dpi=300)

iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 2)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
    # the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 3, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 4)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
# the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 5, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

# Ok, let us do some more iterations. 36 will do.
iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 6)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
# the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 7, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

# Ok, let us do some more iterations. 36 will do.
iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 8)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
# the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 9, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

# Ok, let us do some more iterations. 36 will do.
iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 10)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
# the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 11, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

# Ok, let us do some more iterations. 36 will do.
iterate(P, 3)

# We note that the error (here only displayed for 3 iterations) is
# already declining. That is a good sign.
# Let us have a look how the probe has developed.
fig = u.plot_storage(P.probe.S.values()[0], 12)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after 9 iterations. We observe that
# the actaul illumination of the sample must be larger than the initial
# guess.

# Looks like the probe is on a good way. How about the object?
fig = u.plot_storage(P.obj.S.values()[0], 13, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after 9 iterations. It is not quite
# clear what object is reconstructed

# Ok, let us do some more iterations. 36 will do.

iterate(P, 36)

# Error is still on a steady descent. Let us look at the final
# reconstructed probe and object.
fig = u.plot_storage(P.probe.S.values()[0], 14)
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed probe after a total of 45 iterations.
# It's a moon !


fig = u.plot_storage(P.obj.S.values()[0], 15, slices='0,120:-120,120:-120')
fig.savefig('ownengine_%d.png' % fig.number, dpi=300)
# Plot of the reconstructed object after a total of 45 iterations.
# It's a bunch of flowers !


# .. [#modes] P. Thibault and A. Menzel, **Nature** 494, 68 (2013)
