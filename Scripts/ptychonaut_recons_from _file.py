from ptypy.core import geometry
from ptypy import utils as u
from ptypy.experiment.user import NumpyScan
import os
import numpy as np

"""
u.verbose.set_level(5)

data = u.Param()
data.dfile = 'thisbetterwork.ptyd'

NPS = NumpyScan(data)

NPS.initialize()


print u.verbose.report(NPS.auto(80), noheader=True)
print u.verbose.report(NPS.auto(80), noheader=True)

"""

p = u.Param()  # root level
p.verbose_level = 5
p.data_type = "single"
p.run = 'foo'
p.io = u.Param()

if 'win' in os.sys.platform:
    p.io.home = "C:\\Users\\jspott\\Desktop"
else:
    p.io.home = "/home/jaime"
p.io.autosave = u.Param()
p.io.autosave.interval = 20
p.io.autoplot = u.Param()
p.io.autoplot.active = False
p.io.interaction = u.Param()
p.io.interaction.active = False

p.scans = u.Param()
p.scans.KM = u.Param()
p.scans.KM.name = 'Full'
p.scans.KM.data = u.Param()
p.scans.KM.data.name = 'NumpyScan'
#p.scans.KM.data.shape = 128
#p.scans.KM.data.num_frames = 128
p.scans.KM.data.load_parallel = 'all'
#p.scans.KM.data.save = None

p.engines = u.Param()
p.engines.engine00 = u.Param()
p.engines.engine00.name = 'ePIE'
p.engines.engine00.numiter = 40
p.engines.engine00.numiter_contiguous = 5

"""
p.engines.engine01 = u.Param()
p.engines.engine01.name = 'ML'
"""

from ptypy.core import Ptycho
P = Ptycho(p, level=5)

fig = u.plot_storage(P.obj.S.values()[0], 20, slices='0,120:-120,120:-120')
fig.savefig('reconstruction_orig.png', dpi=300)

#fig = u.plot_error(P, 21)
#fig.savefig('error.png', dpi=300)