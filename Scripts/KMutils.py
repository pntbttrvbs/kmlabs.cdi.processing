#make sure to use the ptycho Py 2.7 env!

import ptypy
from ptypy import utils as u
from ptypy.core import View, Container, Storage, Base, POD, geometry
import sys
from scipy import optimize
import numpy as np

def resolutionCalculator_sim(wavelength = 1.3e-8, distance = 1, psize = 13.5e-6):
    P = Base()

    g = u.Param()
    g.energy = None  # do I need to explicitly declare this?
    g.lam = wavelength
    g.distance = distance
    g.psize = psize
    g.shape = 2048
    g.propagation = 'farfield'  # but... it's pretty close, isn't it? maybe run simulations under both conditions

    G = geometry.Geo(owner=P, pars=g)

    return G.resolution


def calcStepSize(beamRadius, overlap = 0.7):

    """
    Calculates the distance required to move a circular spot of radius beamRadius and keep an area overlap% (overlap).

    :param beamRadius: radius of the circular beam
    :param overlap: area of overlap between original and final beam positions. assuming circular beam.
    :return: distance between initial and final spot center
    """

    def _circleOverlap(radius=1):
        """
        Verified from http://mathworld.wolfram.com/Circle-CircleIntersection.html
        :param radius:
        :return:
        """

        d = radius
        return (((2 * np.arccos(d / 2)) - (d * np.sqrt(1 - ((d * d) / 4)))) / np.pi) - overlap

    if overlap > 100 or overlap < 0:
        raise ValueError('Overlap value not possible.')
    if overlap > 1:
        overlap /= 100
    stepFraction = optimize.broyden1(_circleOverlap, 1)
    return stepFraction * beamRadius

def _spiral_archimedes(a, n):
    """
    Creates np points spiral of step a, with a between successive points
    on the spiral. Returns the x,y coordinates of the spiral points.

    This is an Archimedes spiral. the equation is:
      r=(a/2*pi)*theta
      the stepsize (radial distance between successive passes) is a
      the curved absciss is: s(theta)=(a/2*pi)*integral[t=0->theta](sqrt(1*t**2))dt

    Author: Vincent Favre-Nicolin, pynx
    """
    vr, vt = [0], [0]
    t = np.pi
    while len(vr) < n:
        vt.append(t)
        vr.append(a * t / (2 * np.pi))
        t += 2 * np.pi / np.sqrt(1 + t ** 2)
    vt, vr = np.array(vt), np.array(vr)
    return vr * np.cos(vt), vr * np.sin(vt)

if __name__ == '__main__':
    print(calcStepSize(1))